# Gallery 3 REST Client
![Gallery 3 REST Client](./screenshot.jpg) 

## Description
Gallery 3 REST Client is a desktop application intended for use with a Gallery 3 based web site. This program allows users to log into their Gallery 3 accounts using the REST API (requires the "REST API Module" to be installed and active on the gallery server). Once logged in, users will be able to browse through albums and thumbnails, upload photos and create new albums. Additionally, this program can also allow users to compare the contents of a remote album to the contents of a local folder in order to determine what they may still need to upload as well as what may have been modified since it was uploaded (this requires the ItemChecksum module to be installed and active on the Gallery server).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This application is based on Microsoft's .Net Framework 3.5. You will need to have this installed on your computer in order for the program to work. This should be installed by default on Windows 7, older OS's may/may not already have it installed. If necessary, you can download this from Microsoft's web page [here](http://msdn.microsoft.com/en-us/netframework/cc378097.aspx).
Otherwise there's nothing to install. Just download the latest zip file, extract all of its contents to a folder somewhere, and run the "Gallery Client.exe" program which will display a login prompt.

When logging in, you may provide either your login name and password, or the REST API key found on your profile page for the Gallery web site.

## History
**Version 1.2.0:**
> - Added Logout button / Closing the album window auto-logs out instead of quiting.
> - Added "Recheck Errors" button to Compare Files window.
> - Fixed window re-size issue with Compare Files window.
> - Updated icon file.
> - Updated Copyright to 2012.
> - Other minor tweaks throughout the program.
> - Released 12 March 2012.
>
> Download: [Version 1.2.0 Binary](/uploads/d0b1b36977501f8d07781e278ca4bfbd/Gallery-3-REST-Client-1.2.0-binary.zip) | [Version 1.2.0 Source](/uploads/06fa810640e69502693c176f363a8158/Gallery-3-REST-Client-1.2.0-source.zip)

**Version 1.1.0:**
> - New:  File downloads (thumbnails, resizes, etc) are now run in a seperate thread from the main program.  This allows the main window to remain more responsive while loading thumbnails.
> - New:  Error message windows now provide more details about the error.
> - New:  Display a count of bad files on folder comparison screen
> - Bug Fix:  When loading the root album for the first time, there's a loop to re-fetch it on error.  Except if the error happens with processing the server response, and not communicating with the server, then the bad response is cached and the app doesn't re-request it.
> - Bug Fix:  When closing out the app with empty cache set to true, sometimes there's an error that it can't delete a cache file because it's still in use.
> - Bug Fix:  Fullscreen doesn't work properly if the window is already maxamized.
> - Bug Fix:  Remove "_" characters from image titles, so IMG_0001.jpg should be IMG 0001
> - Bug Fix:  File comparison screen doesn't close local files after generating a checksum.
> - Bug Fix:  Now works fine with Gallery installs that use url_suffix to set a dummy file extension (like .html)
> - Released 18 May 2011.
>
> Download: [Version 1.1.0 Binary](/uploads/b1468387c22c9dcbc1e5aa290623c52f/Gallery-3-REST-Client-1.1.0-binary.zip) | [Version 1.1.0 Source](/uploads/edc5ebdc8451d981184e3401e469ed57/Gallery-3-REST-Client-1.1.0-source.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 04 November 2010
>
> Download: [Version 1.0.0 Binary](/uploads/30d789b7228a271dc0f3314b5eeb5e55/Gallery-3-REST-Client-1.0.0-binary.zip) | [Version 1.0.0 Source](/uploads/88125b463dcde4babdd66c4faea6669f/Gallery-3-REST-Client-1.0.0-source.zip)
